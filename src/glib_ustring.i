/* -----------------------------------------------------------------------------
 * Based on stdtring.i
 *
 * See the LICENSE file for information on copyright, usage and redistribution
 * of SWIG, and the README file for authors - http://www.swig.org/release.html.
 *
 * glib_ustring.i
 *
 * Typemaps for Glib::ustring and const Glib::ustring&
 * These are mapped to a Java String and are passed around by value.
 *
 * To use non-const Glib::ustring references use the following %apply.  Note 
 * that they are passed by value.
 * %apply const Glib::ustring & {Glib::ustring &};
 * ----------------------------------------------------------------------------- */

%{
#include <glibmm/ustring.h>
%}

namespace Glib {

%naturalvar ustring;

class ustring;

// ustring
%typemap(jni) ustring "jstring"
%typemap(jtype) ustring "String"
%typemap(jstype) ustring "String"
%typemap(javadirectorin) ustring "$jniinput"
%typemap(javadirectorout) ustring "$javacall"

%typemap(in) ustring 
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null Glib::ustring");
     return $null;
    } 
    const char *$1_pstr = (const char *)jenv->GetStringUTFChars($input, 0); 
    if (!$1_pstr) return $null;
    $1.assign($1_pstr);
    jenv->ReleaseStringUTFChars($input, $1_pstr); %}

%typemap(directorout) ustring 
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null Glib::ustring");
     return $null;
   } 
   const char *$1_pstr = (const char *)jenv->GetStringUTFChars($input, 0); 
   if (!$1_pstr) return $null;
   $result.assign($1_pstr);
   jenv->ReleaseStringUTFChars($input, $1_pstr); %}

%typemap(directorin,descriptor="Ljava/lang/String;") ustring 
%{ $input = jenv->NewStringUTF($1.c_str()); %}

%typemap(out) ustring 
%{ $result = jenv->NewStringUTF($1.c_str()); %}

%typemap(javain) ustring "$javainput"

%typemap(javaout) ustring {
    return $jnicall;
  }

%typemap(typecheck) ustring = char *;

%typemap(throws) ustring
%{ SWIG_JavaThrowException(jenv, SWIG_JavaRuntimeException, $1.c_str());
   return $null; %}

// const ustring &
%typemap(jni) const ustring & "jstring"
%typemap(jtype) const ustring & "String"
%typemap(jstype) const ustring & "String"
%typemap(javadirectorin) const ustring & "$jniinput"
%typemap(javadirectorout) const ustring & "$javacall"

%typemap(in) const ustring &
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null Glib::ustring");
     return $null;
   }
   const char *$1_pstr = (const char *)jenv->GetStringUTFChars($input, 0); 
   if (!$1_pstr) return $null;
   Glib::ustring $1_str($1_pstr);
   $1 = &$1_str;
   jenv->ReleaseStringUTFChars($input, $1_pstr); %}

%typemap(directorout,warning=SWIGWARN_TYPEMAP_THREAD_UNSAFE_MSG) const ustring &
%{ if(!$input) {
     SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null Glib::ustring");
     return $null;
   }
   const char *$1_pstr = (const char *)jenv->GetStringUTFChars($input, 0); 
   if (!$1_pstr) return $null;
   /* possible thread/reentrant code problem */
   static Glib::ustring $1_str;
   $1_str = $1_pstr;
   $result = &$1_str;
   jenv->ReleaseStringUTFChars($input, $1_pstr); %}

%typemap(directorin,descriptor="Ljava/lang/String;") const ustring &
%{ $input = jenv->NewStringUTF($1.c_str()); %}

%typemap(out) const ustring & 
%{ $result = jenv->NewStringUTF($1->c_str()); %}

%typemap(javain) const ustring & "$javainput"

%typemap(javaout) const ustring & {
    return $jnicall;
  }

%typemap(typecheck) const ustring & = char *;

%typemap(throws) const ustring &
%{ SWIG_JavaThrowException(jenv, SWIG_JavaRuntimeException, $1.c_str());
   return $null; %}

}

