/*
 * This is based on the Swig example found in Examples/java/typemap/example.i.
 *
 * The heavy lifting for this solution was originally done in this thread:
 *
 * http://comments.gmane.org/gmane.comp.programming.swig/1233
 *
 */

/* Define the types to use in the generated JNI C++ code and Java code */
%typemap(jni) Glib::ustring& SBUF "jobject"
%typemap(jtype) Glib::ustring& SBUF "StringBuffer"
%typemap(jstype) Glib::ustring& SBUF "StringBuffer"

/* How to convert Java(JNI) type to requested Glib::ustring type */
%typemap(in) Glib::ustring& SBUF {

  $1 = NULL;
  if($input != NULL) {
    /* Get the String from the StringBuffer */
    jmethodID setLengthID;
    jclass sbufClass = jenv->GetObjectClass($input);
    jmethodID toStringID = jenv->GetMethodID(sbufClass, "toString", "()Ljava/lang/String;");
    jstring js = (jstring) jenv->CallObjectMethod($input, toStringID);

    /* Convert the String to a Glom::ustring string */
    const char *pCharStr = jenv->GetStringUTFChars(js, 0);
    $1 = new Glib::ustring(pCharStr);

    /* Release the UTF string we obtained with GetStringUTFChars */
    jenv->ReleaseStringUTFChars(js, pCharStr);

    /* Zero the original StringBuffer, so we can replace it with the result */
    setLengthID = jenv->GetMethodID(sbufClass, "setLength", "(I)V");
    jenv->CallVoidMethod($input, setLengthID, (jint) 0);
  }
}

/* How to convert the Glib::ustring type to the Java(JNI) type */
%typemap(argout) Glib::ustring& SBUF {

  if($1 != NULL) {
    /* Append the result to the empty StringBuffer */
    jstring newString = jenv->NewStringUTF($1->c_str());
    jclass sbufClass = jenv->GetObjectClass($input);
    jmethodID appendStringID = jenv->GetMethodID(sbufClass, "append", "(Ljava/lang/String;)Ljava/lang/StringBuffer;");
    jenv->CallObjectMethod($input, appendStringID, newString);

    /* Clean up the string object, no longer needed */
    delete $1;
    $1 = NULL;
  }
}
/* Prevent the default freearg typemap from being used */
%typemap(freearg) Glib::ustring& SBUF ""

/* Convert the jstype to jtype typemap type */
%typemap(javain) Glib::ustring& SBUF "$javainput"
