/*
 * Copyright (C) 2009, 2010, 2011 Openismus GmbH
 *
 * This file is part of Java-libglom.
 *
 * Java-libglom is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Java-libglom is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java-libglom.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.glom.libglom.examples;

import java.io.File;

import org.glom.libglom.Document;
import org.glom.libglom.Field;
import org.glom.libglom.FieldVector;
import org.glom.libglom.Glom;
import org.glom.libglom.LayoutGroup;
import org.glom.libglom.LayoutGroupVector;
import org.glom.libglom.LayoutItem;
import org.glom.libglom.LayoutItemVector;
import org.glom.libglom.Relationship;
import org.glom.libglom.RelationshipVector;
import org.glom.libglom.StringVector;

public class ExampleDocumentLoad {

  private static String locale = ""; //This means the original locale.

	private static void printLayoutGroup(LayoutGroup layoutGroup, String indent) {
		if (layoutGroup == null)
			return;

		// Look at each child item:
		LayoutItemVector layoutItems = layoutGroup.get_items();
		for (int i = 0; i < layoutItems.size(); i++) {
			LayoutItem layoutItem = layoutItems.get(i);

			if (layoutItem == null)
				continue;

			System.out.print(indent + "Layout Item: title=" + layoutItem.get_title_or_name(locale) + ", item type="
					+ layoutItem.get_part_type_name());

			String displayName = layoutItem.get_layout_display_name();
			if (displayName.length() > 0)
				System.out.print(" (" + displayName + ")");

			System.out.println();

			// Recurse into child groups:
			LayoutGroup group = LayoutGroup.cast_dynamic(layoutItem);
			if (group != null) {
				printLayoutGroup(group, indent + "  ");
			}

		}
	}

	private static void printLayout(LayoutGroupVector layoutGroups) {

		for (int i = 0; i < layoutGroups.size(); i++) {
			LayoutGroup layoutGroup = layoutGroups.get(i);
			if (layoutGroup == null)
				continue;

			System.out.println("    Layout Group: title=" + layoutGroup.get_title_or_name(locale));
			printLayoutGroup(layoutGroup, "      ");
		}
	}

	private static void printUsage() {
		System.out.println();
		System.out.println("Usage:");
		System.out.println("java " + ExampleDocumentLoad.class.getName() + " example_glom_file.glom");
		System.out.println();
	}

	/**
	 * A port of example_document_load.cc.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		File exampleFile = null;

		if (args.length == 0) {
			String guessedFilename = Glom.GLOM_EXAMPLE_FILE_DIR + File.separator + "example_music_collection.glom";
			System.out.println("No glom file specified. Attempting to use example file:");
			System.out.println("  " + guessedFilename);
			exampleFile = new File(guessedFilename);
			if (!exampleFile.exists()) {
				System.out.println("The file doesn't exist.");
				printUsage();
				System.exit(0);
			}
		} else if (args.length == 1) {
			exampleFile = new File(args[0]);
			if (!exampleFile.exists()) {
				System.out.println("Can't find glom file:");
				System.out.println("  " + args[0]);
				printUsage();
				System.exit(1);
			}
		} else {
			printUsage();
			System.exit(0);
		}

		Glom.libglom_init();
		Document document = new Document();
		String documentURI = "file://" + exampleFile.getAbsolutePath();
		System.out.println("URI=" + documentURI);
		document.set_file_uri(documentURI);

		int failure_code = 0;
		boolean test = document.load(failure_code);
		System.out.println("Load Failure Code=" + failure_code);
		System.out.println("Document load result=" + test);

		if (test == false) {
			System.out.println("Error loading document " + documentURI);
			System.exit(1);
		}

		System.out.println("Database Title: " + document.get_database_title_original());
		System.out.println("Default Table: " + document.get_default_table());

		// Look at each table:
		StringVector table_names = document.get_table_names();
		for (int i = 0; i < table_names.size(); i++) {
			String table_name = table_names.get(i);
			System.out.println("Table: " + table_name);

			// List the fields for this table:
			FieldVector fields = document.get_table_fields(table_name);
			for (int j = 0; j < fields.size(); j++) {

				Field field = fields.get(j);
				if (field == null) {
					continue;
				}

				System.out.println("  Field: name=" + field.get_name() + ", title=" + field.get_title_or_name(locale)
						+ ", type=" + Field.get_type_name_ui(field.get_glom_type()));

			}

			// List the relationships for this table:
			RelationshipVector relationships = document.get_relationships(table_name);
			for (int j = 0; j < relationships.size(); j++) {

				Relationship relationship = relationships.get(j);
				if (relationship == null) {
					continue;
				}

				System.out.println("  Relationship: from field=" + relationship.get_from_field() + ", to table="
						+ relationship.get_to_table() + ", to field=" + relationship.get_to_field());
			}

			// Show the layouts for this table:
			LayoutGroupVector layoutList = document.get_data_layout_groups("list", table_name);
			System.out.println("  Layout: List:");
			printLayout(layoutList);
			LayoutGroupVector layoutDetails = document.get_data_layout_groups("details", table_name);
			System.out.println("  Layout: Details:");
			printLayout(layoutDetails);
		}

		Glom.libglom_deinit();
	}
}
