/* Start of modified shared_ptr.i */ 

// The main implementation detail in using this smart pointer of a type is to customise the code generated
// to use a pointer to the smart pointer of the type, rather than the usual pointer to the underlying type.
// So for some type T, shared_ptr<T> * is used rather than T *.

namespace Glom {
    template <class T> class sharedptr {
    };
}

%fragment("SWIG_null_deleter", "header") {
%#define SWIG_NO_NULL_DELETER_0
%#define SWIG_NO_NULL_DELETER_1
%#define SWIG_NO_NULL_DELETER_SWIG_POINTER_NEW
%#define SWIG_NO_NULL_DELETER_SWIG_POINTER_OWN
}

// Workaround empty first macro argument bug
#define SWIGEMPTYHACK
// Main user macro for defining sharedptr typemaps for both const and non-const pointer types
%define %glom_sharedptr(TYPE...)
%feature("smartptr", noblock=1) TYPE { Glom::sharedptr< TYPE > }
GLOM_SHAREDPTR_TYPEMAPS(SWIGEMPTYHACK, TYPE)
GLOM_SHAREDPTR_TYPEMAPS(const, TYPE)
%enddef

/* Start of modified boost_shared_ptr.i */

// Language specific macro implementing all the customisations for handling the smart pointer
%define GLOM_SHAREDPTR_TYPEMAPS(CONST, TYPE...)

// %naturalvar is as documented for member variables
%naturalvar TYPE;
%naturalvar Glom::sharedptr< CONST TYPE >;

// destructor wrapper customisation
%feature("unref") TYPE 
//"if (debug_shared) { cout << \"deleting use_count: \" << (*smartarg1).use_count() << \" [\" << (boost::get_deleter<SWIG_null_deleter>(*smartarg1) ? std::string(\"CANNOT BE DETERMINED SAFELY\") : ( (*smartarg1).get() ? (*smartarg1)->getValue() : std::string(\"NULL PTR\") )) << \"]\" << endl << flush; }\n"
                               "(void)arg1; delete smartarg1;"

// Typemap customisations...

// plain value
%typemap(in) CONST TYPE ($&1_type argp = 0) %{
  argp = (*(Glom::sharedptr< CONST TYPE > **)&$input) ? (*(Glom::sharedptr< CONST TYPE > **)&$input)->obj() : 0;
  if (!argp) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "Attempt to dereference null $1_type");
    return $null;
  }
  $1 = *argp; %}
%typemap(out) CONST TYPE 
%{ *(Glom::sharedptr< CONST TYPE > **)&$result = new Glom::sharedptr< CONST TYPE >(new $1_ltype(($1_ltype &)$1)); %}

// plain pointer
%typemap(in) CONST TYPE * (Glom::sharedptr< CONST TYPE > *smartarg = 0) %{
  smartarg = *(Glom::sharedptr< CONST TYPE > **)&$input;
  $1 = (TYPE *)(smartarg ? smartarg->obj() : 0); %}
%typemap(out, fragment="SWIG_null_deleter") CONST TYPE * %{
  *(Glom::sharedptr< CONST TYPE > **)&$result = $1 ? new Glom::sharedptr< CONST TYPE >($1 SWIG_NO_NULL_DELETER_$owner) : 0;
%}

// plain reference
%typemap(in) CONST TYPE & %{
  $1 = ($1_ltype)((*(Glom::sharedptr< CONST TYPE > **)&$input) ? (*(Glom::sharedptr< CONST TYPE > **)&$input)->obj() : 0);
  if (!$1) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "$1_type reference is null");
    return $null;
  } %}
%typemap(out, fragment="SWIG_null_deleter") CONST TYPE &
%{ *(Glom::sharedptr< CONST TYPE > **)&$result = new Glom::sharedptr< CONST TYPE >($1 SWIG_NO_NULL_DELETER_$owner); %}

// plain pointer by reference
%typemap(in) TYPE *CONST& ($*1_ltype temp = 0)
%{ temp = (TYPE *)((*(Glom::sharedptr< CONST TYPE > **)&$input) ? (*(Glom::sharedptr< CONST TYPE > **)&$input)->obj() : 0);
   $1 = &temp; %}
%typemap(out, fragment="SWIG_null_deleter") TYPE *CONST&
%{ *(Glom::sharedptr< CONST TYPE > **)&$result = new Glom::sharedptr< CONST TYPE >(*$1 SWIG_NO_NULL_DELETER_$owner); %}

// shared_ptr by value
%typemap(in) Glom::sharedptr< CONST TYPE > ($&1_type argp)
%{ argp = *($&1_ltype*)&$input; 
   if (argp) $1 = *argp; %}
%typemap(out) Glom::sharedptr< CONST TYPE >
%{ *($&1_ltype*)&$result = $1 ? new $1_ltype($1) : 0; %}

// shared_ptr by reference
%typemap(in) Glom::sharedptr< CONST TYPE > & ($*1_ltype tempnull)
%{ $1 = $input ? *($&1_ltype)&$input : &tempnull; %}
%typemap(out) Glom::sharedptr< CONST TYPE > &
%{ *($&1_ltype)&$result = *$1 ? new $*1_ltype(*$1) : 0; %} 

// shared_ptr by pointer
%typemap(in) Glom::sharedptr< CONST TYPE > * ($*1_ltype tempnull)
%{ $1 = $input ? *($&1_ltype)&$input : &tempnull; %}
%typemap(out) Glom::sharedptr< CONST TYPE > *
%{ *($&1_ltype)&$result = ($1 && *$1) ? new $*1_ltype(*$1) : 0;
   if ($owner) delete $1; %}

// shared_ptr by pointer reference
%typemap(in) Glom::sharedptr< CONST TYPE > *& (Glom::sharedptr< CONST TYPE > tempnull, $*1_ltype temp = 0)
%{ temp = $input ? *($1_ltype)&$input : &tempnull;
   $1 = &temp; %}
%typemap(out) Glom::sharedptr< CONST TYPE > *&
%{ *($1_ltype)&$result = (*$1 && **$1) ? new Glom::sharedptr< CONST TYPE >(**$1) : 0; %} 

// various missing typemaps - If ever used (unlikely) ensure compilation error rather than runtime bug
%typemap(in) CONST TYPE[], CONST TYPE[ANY], CONST TYPE (CLASS::*) %{
#error "typemaps for $1_type not available"
%}
%typemap(out) CONST TYPE[], CONST TYPE[ANY], CONST TYPE (CLASS::*) %{
#error "typemaps for $1_type not available"
%}


%typemap (jni)    Glom::sharedptr< CONST TYPE >, 
                  Glom::sharedptr< CONST TYPE > &,
                  Glom::sharedptr< CONST TYPE > *,
                  Glom::sharedptr< CONST TYPE > *& "jlong"
%typemap (jtype)  Glom::sharedptr< CONST TYPE >, 
                  Glom::sharedptr< CONST TYPE > &,
                  Glom::sharedptr< CONST TYPE > *,
                  Glom::sharedptr< CONST TYPE > *& "long"
%typemap (jstype) Glom::sharedptr< CONST TYPE >, 
                  Glom::sharedptr< CONST TYPE > &,
                  Glom::sharedptr< CONST TYPE > *,
                  Glom::sharedptr< CONST TYPE > *& "$typemap(jstype, TYPE)"

%typemap(javain) Glom::sharedptr< CONST TYPE >, 
                 Glom::sharedptr< CONST TYPE > &,
                 Glom::sharedptr< CONST TYPE > *,
                 Glom::sharedptr< CONST TYPE > *& "$typemap(jstype, TYPE).getCPtr($javainput)"

%typemap(javaout) Glom::sharedptr< CONST TYPE > {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }
%typemap(javaout) Glom::sharedptr< CONST TYPE > & {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }
%typemap(javaout) Glom::sharedptr< CONST TYPE > * {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }
%typemap(javaout) Glom::sharedptr< CONST TYPE > *& {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }


%typemap(javaout) CONST TYPE {
    return new $typemap(jstype, TYPE)($jnicall, true);
  }
%typemap(javaout) CONST TYPE & {
    return new $typemap(jstype, TYPE)($jnicall, true);
  }
%typemap(javaout) CONST TYPE * {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }
%typemap(javaout) TYPE *CONST& {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new $typemap(jstype, TYPE)(cPtr, true);
  }

// Base proxy classes
%typemap(javabody) TYPE %{
  private long swigCPtr;
  private boolean swigCMemOwnBase;

  public $javaclassname(long cPtr, boolean cMemoryOwn) {
    swigCMemOwnBase = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr($javaclassname obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }
%}

// Derived proxy classes
%typemap(javabody_derived) TYPE %{
  private long swigCPtr;
  private boolean swigCMemOwnDerived;

  public $javaclassname(long cPtr, boolean cMemoryOwn) {
    super($imclassname.$javaclazznameSWIGSmartPtrUpcast(cPtr), true);
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr($javaclassname obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }
%}

%typemap(javadestruct, methodname="delete", methodmodifiers="public synchronized") TYPE {
    if (swigCPtr != 0) {
      if (swigCMemOwnBase) {
        swigCMemOwnBase = false;
        $jnicall;
      }
      swigCPtr = 0;
    }
  }

%typemap(javadestruct_derived, methodname="delete", methodmodifiers="public synchronized") TYPE {
    if (swigCPtr != 0) {
      if (swigCMemOwnDerived) {
        swigCMemOwnDerived = false;
        $jnicall;
      }
      swigCPtr = 0;
    }
    super.delete();
  }


%template() Glom::sharedptr< CONST TYPE >;
%enddef

