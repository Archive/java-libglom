/*
 * Copyright (C) 2011 Openismus GmbH
 *
 * This file is part of GWT-Glom.
 *
 * GWT-Glom is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * GWT-Glom is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GWT-Glom.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.glom.libglom.tests;

import static org.junit.Assert.assertTrue;

import org.glom.libglom.Document;
import org.glom.libglom.Field;
import org.glom.libglom.Glom;
import org.glom.libglom.LayoutFieldVector;
import org.glom.libglom.LayoutItem_Field;
import org.glom.libglom.SqlBuilder;
import org.glom.libglom.SqlExpr;
import org.glom.libglom.Value;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Ported from:
 * 
 * http://git.gnome.org/browse/glom/tree/tests/test_fake_connection.cc
 * 
 * @author Murry Cumming
 * @author Ben Konrath <ben@bagu.org>
 */
public class FakeConnectionTest {
	private static Document document;

	@BeforeClass
	static public void setUp() {
		Glom.libglom_init();
		document = new Document();
		document.set_file_uri("file://" + Glom.GLOM_EXAMPLE_FILE_DIR + "/example_music_collection.glom");
		int failure_code = 0;
		boolean test = document.load(failure_code);
		assertTrue("Document.load() failed with failure_code = " + failure_code, test);

		// Allow a fake connection, so sqlbuilder_get_full_query() can work:
		Glom.set_fake_connection();
	}

	@AfterClass
	static public void tearDown() {
		Glom.libglom_deinit();
	}

	@Test
	public void testFakeConnection() {
		// Build a SQL query and get the string for it:
		Value value = new Value("Born To Run");
		Field where_field = document.get_field("albums", "name");
		SqlExpr where_clause = Glom.build_simple_where_expression("albums", where_field, value);

		LayoutFieldVector fieldsToGet = new LayoutFieldVector();
		Field field = document.get_field("albums", "album_id");
		LayoutItem_Field layoutitem = new LayoutItem_Field();
		layoutitem.set_full_field_details(field);
		fieldsToGet.add(layoutitem);

		field = document.get_field("albums", "name");
		layoutitem = new LayoutItem_Field();
		layoutitem.set_full_field_details(field);
		fieldsToGet.add(layoutitem);

		SqlBuilder builder = Glom.build_sql_select_with_where_clause("albums", fieldsToGet, where_clause);
		String query = Glom.sqlbuilder_get_full_query(builder);
		assertTrue(!query.isEmpty());
		assertTrue("Failed: The query did not contain an expected field name.", query.contains("album_id"));
	}

}
