/*
 * This file wraps the parts of libgdamm that we want to use in java-libglom.
 * TODO: Figure out how to add this its own package (org.glom.libglom.gda).
 */

/*
 * The header files to include. This is not strictly necessary but it makes
 * this interface file more modular.
 */
%{
#include <libgdamm/value.h>
#include <libgdamm/sqlexpr.h>
#include <libgdamm/sqlbuilder.h>
%}

/*
 * Gnome::Gda::Value.
 */
// Things to ignore.
%ignore Gnome::Gda::Value::Value(const GValue* castitem);
%ignore Gnome::Gda::Value::create_as_int64(gint64 val);
%ignore Gnome::Gda::Value::create_as_uint64(guint64 val);
%ignore Gnome::Gda::Value::Value(const guchar* val, long size);
%ignore Gnome::Gda::Value::Value(const GdaBlob* val);
%ignore Gnome::Gda::Value::Value(const Glib::Date& val);
%ignore Gnome::Gda::Value::Value(const GeometricPoint& val);
%ignore Gnome::Gda::Value::Value(int val);
%ignore Gnome::Gda::Value::Value(const Numeric& val);
%ignore Gnome::Gda::Value::Value(float val);
%ignore Gnome::Gda::Value::Value(gshort val);
%ignore Gnome::Gda::Value::Value(gushort val);
%ignore Gnome::Gda::Value::Value(gulong val);
%ignore Gnome::Gda::Value::Value(const char* val);
%ignore Gnome::Gda::Value::Value(const Time& val);
%ignore Gnome::Gda::Value::Value(const Timestamp& val);
%ignore Gnome::Gda::Value::create_as_time_t(time_t val);
%ignore Gnome::Gda::Value::Value(gchar val);
%ignore Gnome::Gda::Value::Value(guchar val);
%ignore Gnome::Gda::Value::Value(guint val);
%ignore Gnome::Gda::Value::Value(const Glib::ustring& as_string, GType type);
%ignore Gnome::Gda::Value::get_value_type() const;
%ignore Gnome::Gda::Value::get_int64() const;
%ignore Gnome::Gda::Value::set_int64(gint64 val);
%ignore Gnome::Gda::Value::get_uint64() const;
%ignore Gnome::Gda::Value::set_uint64(guint64 val);
%ignore Gnome::Gda::Value::get_binary(long& size) const;
%ignore Gnome::Gda::Value::set(const guchar* val, long size);
%ignore Gnome::Gda::Value::get_blob() const;
%ignore Gnome::Gda::Value::set(const GdaBlob* val);
%ignore Gnome::Gda::Value::get_date() const;
%ignore Gnome::Gda::Value::set(const Glib::Date& val);
%ignore Gnome::Gda::Value::get_geometric_point() const;
%ignore Gnome::Gda::Value::set(const GeometricPoint& val);
%ignore Gnome::Gda::Value::get_gobject();
%ignore Gnome::Gda::Value::set(const Glib::RefPtr<Glib::Object>& val);
%ignore Gnome::Gda::Value::get_int() const;
%ignore Gnome::Gda::Value::set(int val);
%ignore Gnome::Gda::Value::get_list();
%ignore Gnome::Gda::Value::get_numeric() const;
%ignore Gnome::Gda::Value::set(const Numeric& val);
%ignore Gnome::Gda::Value::get_float() const;
%ignore Gnome::Gda::Value::set(float val);
%ignore Gnome::Gda::Value::get_short() const;
%ignore Gnome::Gda::Value::set(gshort val);
%ignore Gnome::Gda::Value::get_ushort() const;
%ignore Gnome::Gda::Value::set(gushort val);
%ignore Gnome::Gda::Value::get_long() const;
%ignore Gnome::Gda::Value::set(glong val);
%ignore Gnome::Gda::Value::get_ulong() const;
%ignore Gnome::Gda::Value::set(gulong val);
%ignore Gnome::Gda::Value::set(const char* val);
%ignore Gnome::Gda::Value::get_time() const;
%ignore Gnome::Gda::Value::set(const Time& val);
%ignore Gnome::Gda::Value::get_timestamp() const;
%ignore Gnome::Gda::Value::set(const Timestamp& val);
%ignore Gnome::Gda::Value::set(gchar val);
%ignore Gnome::Gda::Value::set(guchar val);
%ignore Gnome::Gda::Value::get_uint() const;
%ignore Gnome::Gda::Value::set(guint val);
%ignore Gnome::Gda::Value::get_g_type() const;
%ignore Gnome::Gda::Value::set_g_type(GType val);
%ignore Gnome::Gda::ValueTraits;
%ignore Gnome::Gda::value_get_type_null();
%ignore Gnome::Gda::value_get_type_binary();
%ignore Gnome::Gda::value_get_type_blob();
%ignore Gnome::Gda::value_get_type_geometric_point();
%ignore Gnome::Gda::value_get_type_list();
%ignore Gnome::Gda::value_get_type_numeric();
%ignore Gnome::Gda::value_get_type_short();
%ignore Gnome::Gda::value_get_type_ushort();
%ignore Gnome::Gda::value_get_type_time();
%ignore Gnome::Gda::value_get_type_timestamp();

// The header file for Gnome::Gda::Value.
%include <libgdamm/value.h>


/*
 * Gnome::Gda::SqlExpr
 */
// Things to ignore.
%ignore Gnome::Gda::SqlExpr::SqlExpr(GdaSqlExpr* gobject, bool make_a_copy = true);
%ignore Gnome::Gda::SqlExpr::gobj();
%ignore Gnome::Gda::SqlExpr::gobj() const;
%ignore Gnome::Gda::SqlExpr::gobj_copy() const;
%ignore Gnome::Gda::SqlExpr::SqlExpr(GdaSqlAnyPart *parent);
%ignore Glib::wrap(GdaSqlExpr* object, bool take_copy = false);
%ignore Gnome::Gda::SqlExpr::get_type();

// The header file for Gnome::Gda::SqlExpr.
// DOXYGEN_SHOULD_SKIP_THIS is defined to avoid this SWIG warning:
//   Warning 317: Specialization of non-template 'Value'.
// If important information is ignored when the Doxygen macro is define, it
// can be removed but '#define G_GNUC_CONST' will need to be added instead.
#define DOXYGEN_SHOULD_SKIP_THIS
%include <libgdamm/sqlexpr.h>
#undef DOXYGEN_SHOULD_SKIP_THIS


/*
 * Gnome::Gda::SqlBuilder
 */
// Ignore everything in the SqlBuilder class.
%rename("$ignore", regextarget=1, fullname=1) "Gnome::Gda::SqlBuilder::*";
%ignore Glib::wrap(GdaSqlBuilder* object, bool take_copy = false);
// Ignore classes that aren't used.
%ignore Gnome::Gda::SqlError;
%ignore Gnome::Gda::SqlBuilderError;
%ignore Gnome::Gda::SqlSelectJoinType;

// Deal with Glib::RefPtr in the same way as Glom::sharedptr.
%include "glib_refptr.i"
%glib_refptr(Gnome::Gda::SqlBuilder);

// The header file for Gnome::Gda::SqlBuilder.
// DOXYGEN_SHOULD_SKIP_THIS is defined to avoid this SWIG warning:
//   Warning 317: Specialization of non-template 'Value'.
// If important information is ignored when the Doxygen macro is define, it
// can be removed but '#define G_GNUC_CONST' will need to be added instead.
#define DOXYGEN_SHOULD_SKIP_THIS
%include <libgdamm/sqlbuilder.h>
#undef DOXYGEN_SHOULD_SKIP_THIS
