/*
 * Copyright (C) 2009, 2010, 2011 Openismus GmbH
 *
 * This file is part of Java-libglom.
 *
 * Java-libglom is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Java-libglom is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java-libglom.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Includes required for compiling the JNI shared object.
 */
%module Glom
%{

// Stick to the client-only API for swig, for now.
// TODO: Watch out if some of these are virtual.
#define GLOM_ENABLE_CLIENT_ONLY 1

#include <libglom/init.h>
#include <libglom/document/document.h>
#include <libglom/data_structure/glomconversions.h>
#include <libglom/data_structure/layout/layoutitem_calendarportal.h>
#include <libglom/data_structure/layout/report_parts/layoutitem_fieldsummary.h>
#include <libglom/data_structure/layout/report_parts/layoutitem_summary.h>
#include <libglom/data_structure/layout/report_parts/layoutitem_verticalgroup.h>
#include <libglom/utils.h>
#include <libglom/db_utils.h>
using namespace Glom;
%}

/*
 * Constants are in a separate .i file to make it easy to set the values during
 * the build.
 */
%include glom_constants.i


/*
 * Wrap C++ enum as proper Java enums by default as recommended in the Swig
 * documentation. Some C++ enums won't work as Java enums in which case they
 * will need to be special cased with '%javacont(0) Class::enum;'
 */
%include "enums.swg"
%javaconst(1);


/*
 * Ignore all methods with these signatures.
 */
%ignore *::operator =;
%ignore *::operator !=;


/*
 * Rename overloaded operator == to .equals().
 */
%rename(equals) *::operator ==;


/*
 * Allows us to use std::string, std::vector, std::map and std::pair
 */
%include <stl.i>


/*
 * Include typemap.i so that we can use %apply.
 */
%include <typemaps.i>
%apply unsigned int { guint };
%apply unsigned long { gulong };


/*
 * Enable various reference parameters.
 */
%apply int &INPUT { int& };
%apply double &INPUT { double& };
%apply bool &INPUT { bool& };
%apply unsigned long &INPUT { gulong& };

/*
 * Enable Glib::ustring.
 */
// This was adapted from Swig's Lib/java/std_string.i for Glib::ustring.
%include "glib_ustring.i"
// This enables Glib::ustring reference parameters using the StringBuffer class
// to transfer the string. This feature needs to be turned on for specific
// variables with an %apply command. See Glom::Formatting below for an
// example. We can't use a generic %apply command like:
//   %apply Glib::ustring& SBUF { Glib::ustring& };
// because it will pick up 'const Glib::ustring&' as well 'Glib::ustring&'
// which is not what we want.
%include "glib_ustring_reference_parameter.i"
%template(StringVector) std::vector< Glib::ustring >;

/*
 * Gnome::Gda interface file. This needs to be below the %apply statements.
 */
%include "libgdamm.i"

/*
 * Typemaps for transparently using Glom::sharedptr.
 */
// This defines the %glom_sharedptr macro used below.
%include "glom_sharedptr.i"

// These macros allow us to transparently use Glom::sharedptr for the given
// class.
%glom_sharedptr(Glom::TranslatableItem);
%glom_sharedptr(Glom::HasTitleSingular);
%glom_sharedptr(Glom::PrintLayout);
%glom_sharedptr(Glom::Report);
%glom_sharedptr(Glom::TableInfo);
%glom_sharedptr(Glom::LayoutItem);
%glom_sharedptr(Glom::Field);
%glom_sharedptr(Glom::Relationship);
%glom_sharedptr(Glom::LayoutGroup);
%glom_sharedptr(Glom::LayoutItem_GroupBy);
%glom_sharedptr(Glom::LayoutItem_Summary);
%glom_sharedptr(Glom::LayoutItem_FieldSummary);
%glom_sharedptr(Glom::LayoutItem_Notebook);
%glom_sharedptr(Glom::LayoutItem_Portal);
%glom_sharedptr(Glom::LayoutItem_CalendarPortal);
%glom_sharedptr(Glom::LayoutItem_WithFormatting);
%glom_sharedptr(Glom::LayoutItem_Field);
%glom_sharedptr(Glom::LayoutItem_Text);
%glom_sharedptr(Glom::LayoutItem_VerticalGroup);
%glom_sharedptr(Glom::CustomTitle);
%glom_sharedptr(Glom::ChoiceValue);


/*
 * Wrap classes for getting Field formatting information.
 *
 * These classes don't work with the sharedptr macro.
 */
// Ignoring UsesRelationship because we not going to use it. See the comment
// about UsesRelationship in the '%extend Glom::LayoutItem_Field' section below.
%ignore Glom::UsesRelationship;

// Enable Glom::NumericFormat
// the %rename makes the getter and setter look better
%rename(_currency_symbol) Glom::NumericFormat::m_currency_symbol;
%rename(_use_thousands_separator) Glom::NumericFormat::m_use_thousands_separator;
%rename(_decimal_places_restricted) Glom::NumericFormat::m_decimal_places_restricted;
%rename(_decimal_places) Glom::NumericFormat::m_decimal_places;
%rename(_alt_foreground_color_for_negatives) Glom::NumericFormat::m_alt_foreground_color_for_negatives;
%include <libglom/data_structure/numeric_format.h>

// Wrap ChoiceValue, used by Formatting:
%include <libglom/data_structure/choicevalue.h>

/*
 * Ignore method overloads that differ only by constness of the parameters,
 * because Java doesn't have const, so it would just generate a duplicate method.
 */
%ignore Glom::Formatting::get_choices_related(sharedptr<const Relationship>& relationship, sharedptr<const LayoutItem_Field>& field, sharedptr<const LayoutGroup>& extra_layout, type_list_sort_fields& sort_fields, bool& show_all) const;

// the %rename makes the getter and setter look better
%rename(_numeric_format) Glom::Formatting::m_numeric_format;
%include <libglom/data_structure/layout/formatting.h>


/*
 * Classes that use Glom::sharedptr. Base classes need to be placed before
 * subclasses.
 *
 * Top-level base classes.
 */
%include <libglom/data_structure/translatable_item.h>
%include <libglom/data_structure/has_title_singular.h>


/*
 * Subclasses of TranslatableItem.
 */
%include <libglom/data_structure/layout/layoutitem.h>

// Ignored because Gnome::Gda::Holder is not wrapped.
%ignore Glom::Field::get_holder(const Gnome::Gda::Value& value, const Glib::ustring& name = Glib::ustring()) const;
// Ignore theses sql escaping methods because gwt-glom doesn't manually create queries.
%ignore Glom::Field::sql(const Gnome::Gda::Value& value, const Glib::RefPtr<Gnome::Gda::Connection>& connection) const;
%ingore Glom::Field::sql(const Gnome::Gda::Value& value) const;
%ignore Glom::Field::sql_find(const Gnome::Gda::Value& value, const Glib::RefPtr<Gnome::Gda::Connection>& connection) const;
%ignore Glom::Field::sql_find_operator() const;
// Ignored because GType is not wrapped.
%ignore Glom::Field::get_glom_type_for_gda_type(GType gda_type);
%ignore Glom::Field::get_gda_type_for_glom_type(Field::glom_field_type glom_type);
// Ignored because Gnome::Gda::Column is not wrapped.
%ignore Glom::Field::field_info_from_database_is_equal(const Glib::RefPtr<const Gnome::Gda::Column>& field);
%ignore Glom::Field::get_field_info();
%ignore Glom::Field::get_field_info() const;
%ignore Glom::Field::set_field_info(const Glib::RefPtr<Gnome::Gda::Column>& fieldInfo);
// Ignored because this method was removed from Glom 1.21/22 and we do not need it:
%ignore Glom::Field::get_gda_holder_name();
// the %rename makes the getter and setter look better
%rename(_default_formatting) Glom::Field::m_default_formatting;
%include <libglom/data_structure/field.h>

%include <libglom/data_structure/relationship.h>
%include <libglom/data_structure/layout/custom_title.h>

// the %rename makes the getters and setters look better
// TODO: This is not working. murrayc.
%rename(_sequence) Glom::FieldInfo::m_sequence;
%rename(_hidden) Glom::FieldInfo::m_hidden;
%rename(_default) Glom::FieldInfo::m_default;
%include <libglom/data_structure/tableinfo.h>

%include <libglom/data_structure/print_layout.h>
%ignore  Glom::Report::get_layout_group() const;
%include <libglom/data_structure/report.h>


/*
 * Subclasses of LayoutItem.
 */
// Ignore const method
%ignore Glom::LayoutGroup::get_items() const;
%ignore Glom::LayoutGroup::get_items_recursive() const;
// Ignored because this method was only added inGlom 1.21/22 and we do not need it yet:
%ignore Glom::LayoutGroup::get_items_recursive_with_groups() const;
%include <libglom/data_structure/layout/layoutgroup.h>
// Ignored so that getters and setters aren't generated for the m_formatting field.
// This can be enabled if it's required.
%ignore Glom::LayoutItem_WithFormatting::m_formatting;
%include <libglom/data_structure/layout/layoutitem_withformatting.h>


/*
 * Subclasses of LayoutGroup.
 */
// LayoutItem_Notebook:
%include <libglom/data_structure/layout/layoutitem_notebook.h>

/** Report layout items */
%ignore Glom::LayoutItem_GroupBy::get_fields_sort_by() const;
%ignore Glom::LayoutItem_GroupBy::get_field_group_by() const;
%rename(_group_secondary_fields) Glom::LayoutItem_GroupBy::m_group_secondary_fields;
%include <libglom/data_structure/layout/report_parts/layoutitem_groupby.h>
%include <libglom/data_structure/layout/report_parts/layoutitem_summary.h>
%include <libglom/data_structure/layout/report_parts/layoutitem_fieldsummary.h>
%include <libglom/data_structure/layout/report_parts/layoutitem_verticalgroup.h>

// LayoutItem_Portal:
//
// We don't need this method for now but a wrapper method can be created to avoid using UsesRelationship.
%ignore Glom::LayoutItem_Portal::set_navigation_relationship_specific(const sharedptr<UsesRelationship>& relationship);

// These methods are being ignored because we're not wrapping UsesRelationship.
// Wrapper methods for these methods have been added below.
%ignore Glom::LayoutItem_Portal::get_navigation_relationship_specific();
%ignore Glom::LayoutItem_Portal::get_suitable_table_to_view_details(Glib::ustring& table_name, sharedptr<const UsesRelationship>& relationship, const Document* document) const;
%ignore Glom::LayoutItem_Portal::get_portal_navigation_relationship_automatic(const Document* document) const;

// Enable Glib::ustring reference parameter for the
// get_suitable_table_to_view_details() method defined below.
%apply Glib::ustring& SBUF { Glib::ustring& suitable_table_name };

// Ignore const method
%ignore Glom::LayoutItem_Portal::get_navigation_relationship_specific() const;

// The actual LayoutItem_Portal class.
%include <libglom/data_structure/layout/layoutitem_portal.h>

// Extend LayoutItem_Portal to use a LayoutItem_Field for the navigation
// relationship information instead of UsesReleationship because we're not
// wrapping UsesRelationship.
%extend Glom::LayoutItem_Portal {

void get_suitable_table_to_view_details(Glib::ustring& suitable_table_name, sharedptr<Glom::LayoutItem_Field>& navigation_relationship_item, const Document* document)
{
  sharedptr<const UsesRelationship> navigation_relationship;
  self->get_suitable_table_to_view_details(suitable_table_name, navigation_relationship, document);

  // create a layout item to set the relationship info
  sharedptr<LayoutItem_Field> layout_item = sharedptr<LayoutItem_Field>::create();
  if(navigation_relationship)
  {
    layout_item->set_relationship( navigation_relationship->get_relationship() );
    layout_item->set_related_relationship( navigation_relationship->get_related_relationship() );
  }

  navigation_relationship_item = layout_item;
}

sharedptr<const LayoutItem_Field> get_portal_navigation_relationship_automatic(const Document* document) const
{
  sharedptr<const UsesRelationship> navigation_relationship =
    self->get_portal_navigation_relationship_automatic(document);

  // create a layout item to set the relationship info
  sharedptr<LayoutItem_Field> relationship_layout_item = sharedptr<LayoutItem_Field>::create();
  if(navigation_relationship)
  {
    relationship_layout_item->set_relationship( navigation_relationship->get_relationship() );
    relationship_layout_item->set_related_relationship( navigation_relationship->get_related_relationship() );
  }

  return relationship_layout_item;
}

sharedptr<const LayoutItem_Field> get_navigation_relationship_specific()
{
  sharedptr<const UsesRelationship> navigation_relationship =
    self->get_navigation_relationship_specific();

  // create a layout item to set the relationship info
  sharedptr<LayoutItem_Field> relationship_layout_item = sharedptr<LayoutItem_Field>::create();
  if(navigation_relationship)
  {
    relationship_layout_item->set_relationship( navigation_relationship->get_relationship() );
    relationship_layout_item->set_related_relationship( navigation_relationship->get_related_relationship() );
  }

  return relationship_layout_item;
}

};


/*
 * Subclasses of LayoutItem_Portal.
 */
// Ignore const method
%ignore Glom::LayoutItem_CalendarPortal::get_date_field() const;
%include <libglom/data_structure/layout/layoutitem_calendarportal.h>


/*
 * Subclasses of LayoutItem_WithFormatting.
 */
// LayoutItem_Field:
%ignore Glom::LayoutItem_Field::get_title_custom() const;
%include <libglom/data_structure/layout/layoutitem_field.h>

// LayoutItem_Text:
// This %rename makes the getter and setter look better.
// Get/set_text() is already used in LayoutItem_Text so we're using
// get/set_translatable_item.
%rename(_translatable_item) Glom::LayoutItem_Text::m_text;
%include <libglom/data_structure/layout/layoutitem_text.h>


/*
 * Wrap the vectors.
 */
%template(FieldVector) std::vector< Glom::sharedptr<Glom::Field> >;
%template(LayoutGroupVector) std::vector< Glom::sharedptr<LayoutGroup> >;
%template(RelationshipVector) std::vector< Glom::sharedptr<Relationship> >;
%template(LayoutItemVector) std::vector< Glom::sharedptr<LayoutItem> >;
%template(LayoutFieldVector) std::vector< Glom::sharedptr<LayoutItem_Field> >;
%template(DoubleVector) std::vector<double>;

/*
 * Wrap the maps.
 */
%template(TypeNameMap) std::map< Glom::Field::glom_field_type, Glib::ustring >;
%template(LocalToTranslationMap) std::map< Glib::ustring, Glib::ustring >;


/*
 * Wrap the libglom initialization methods.
 */
%include <libglom/init.h>


/*
 * Wrap Glom::Document and include required base classes.
 */
// Ignored to avoid wrapping signals
%ignore GlomBakery::Document::signal_modified();
%ignore GlomBakery::Document::signal_forget();
// If this method is needed, we'll need to support guchar* - %apply could help here.
%ignore GlomBakery::Document::load_from_data(const guchar* data, std::size_t length, int& failure_code);
%rename(BakeryDocument) GlomBakery::Document;
%rename(BakeryDocumentXML) GlomBakery::Document_XML;
%ignore GlomBakery::Document::set_view;
%ignore GlomBakery::Document::get_view;
%javaconst(0) Glom::Document::LOAD_FAILURE_CODE_FILE_VERSION_TOO_NEW;
%include <libglom/document/bakery/document.h>
%include <libglom/document/bakery/document_xml.h>

// the %rename makes the getters and setters look better
%rename(_table_name) Glom::FoundSet::m_table_name;
%rename(_extra_join) Glom::FoundSet::m_extra_join;
%rename(_where_clause) Glom::FoundSet::m_where_clause;
%rename(_sort_clause) Glom::FoundSet::m_sort_clause;
%include <libglom/data_structure/foundset.h>

// Ignore these methods that are not currently used by gwt-glom.
%ignore Glom::Document::get_layout_record_viewed(const Glib::ustring& table_name, const Glib::ustring& layout_name) const;
%ignore Glom::Document::set_table_example_data(const Glib::ustring& table_name, const type_example_rows& rows);
%ignore Glom::Document::get_table_example_data(const Glib::ustring& table_name) const;
// Ignore the GroupInfo class and methods for now because the shareptr class
// is causing problems. These are only used for recreating a database from an
// example file so we shouldn't need them for now.
%ignore Glom::GroupInfo;
%ignore Glom::Document::set_group;
%ignore Glom::Document::get_groups;
%ignore Glom::Document::remove_group;
// Ignore get_lookup_fields() until we wrap type_list_lookups
%ignore Glom::Document::get_lookup_fields;
%include <libglom/document/document.h>


/*
 * Wrap Glom::Utils.
 * TODO: Figure out a way to add the Glom::Utils methods to a Utils class in Java.
 */
// Ignore methods with Gio::File.
%ignore Glom::Utils::file_exists(const Glib::RefPtr<Gio::File>& file);
%ignore Glom::Utils::delete_directory(const Glib::RefPtr<Gio::File>& directory);
// Ignore the const version of these to build_sql methods as we're using the non-const versions.
%ignore Glom::Utils::build_sql_select_with_where_clause(
  const Glib::ustring& table_name,
  const type_vecConstLayoutFields& fieldsToGet,
  const Gnome::Gda::SqlExpr& where_clause = Gnome::Gda::SqlExpr(),
  const sharedptr<const Relationship>& extra_join = sharedptr<const Relationship>(),
  const type_sort_clause& sort_clause = type_sort_clause(),
  guint limit = 0);
%ignore Glom::Utils::build_sql_select_with_key(
  const Glib::ustring& table_name,
  const type_vecConstLayoutFields& fieldsToGet,
  const sharedptr<const Field>& key_field,
  const Gnome::Gda::Value& key_value,
  const type_sort_clause& sort_clause = type_sort_clause(),
  guint limit = 0);
// Ignore this method because it uses type_vecConstLayoutFields which is not currently wrapped.
%ignore build_sql_select_add_fields_to_get(
  const Glib::RefPtr<Gnome::Gda::SqlBuilder>& builder,
  const Glib::ustring& table_name,
  const type_vecConstLayoutFields& fieldsToGet,
  const type_sort_clause& sort_clause,
  bool extra_join);
%ignore get_layout_items_plus_primary_key(
  const LayoutGroup::type_list_const_items& items,
  const Document* document,
  const Glib::ustring& table_name);
%include <libglom/utils.h>


/*
 * Wrap classes required to setup a sort clause.
 */
%template(SortFieldPair) std::pair< Glom::sharedptr<const LayoutItem_Field>, bool>;

%template(SortClause) std::vector<std::pair< Glom::sharedptr<const LayoutItem_Field>, bool> >;


/*
 * Wrap some other lists.
 */
%template(TranslatableItemPair) std::pair< Glom::sharedptr<TranslatableItem>, Glib::ustring>;
%template(TranslatableItemVector) std::vector<std::pair< Glom::sharedptr<TranslatableItem>, Glib::ustring> >;
%template(TableInfoVector) std::vector< Glom::sharedptr<TableInfo> >;
%template(ChoiceValueVector) std::vector< Glom::sharedptr<ChoiceValue> >;


/*
 * Wrap Gnome::Gda::Value lists and pairs. These are defined in Glom::Utils.
 */
%template(ValueVector) std::vector< Gnome::Gda::Value >;
%template(ValueVectorPair) std::pair< Gnome::Gda::Value, std::vector<Gnome::Gda::Value> >;
%template(ValueWithSecondVector) std::vector< std::pair< Gnome::Gda::Value, std::vector<Gnome::Gda::Value> > >;


/*
 * Wrap Glom::DbUtils.
 * TODO: Figure out a way to add the Glom::DbUtils methods to a DbUtils class in Java.
 */
// Ignore methods we don't need right now or with parameters we're not wrapping.
%ignore Glom::DbUtils::create_database(Document* document, const Glib::ustring& database_name, const Glib::ustring& title, const sigc::slot<void>& progress);
%ignore Glom::DbUtils::recreate_database_from_document(Document* document, const sigc::slot<void>& progress);
%ignore Glom::DbUtils::get_database_preferences(Document* document);
%ignore Glom::DbUtils::set_database_preferences(Document* document, const SystemPrefs& prefs);
%ignore Glom::DbUtils::add_column(const Glib::ustring& table_name, const sharedptr<const Field>& field, Gtk::Window* parent_window);
%ignore Glom::DbUtils::query_execute_select(const Glib::RefPtr<const Gnome::Gda::SqlBuilder>& builder, bool use_cursor = false);
%ignore Glom::DbUtils::query_execute_string(const Glib::ustring& strQuery, const Glib::RefPtr<Gnome::Gda::Set>& params);
%ignore Glom::DbUtils::query_execute(const Glib::RefPtr<const Gnome::Gda::SqlBuilder>& builder);
%ignore Glom::DbUtils::auto_increment_insert_first_if_necessary(const Glib::ustring& table_name, const Glib::ustring& field_name);
%ignore Glom::DbUtils::get_next_auto_increment_value(const Glib::ustring& table_name, const Glib::ustring& field_name);
%ignore Glom::DbUtils::count_rows_returned_by(const Glib::RefPtr<const Gnome::Gda::SqlBuilder>& sql_query);
%include <libglom/db_utils.h>


/*
 * cast_dynamic methods.
 *
 * The code for cast_dynamic methods has been taken from the template code in
 * sharedptr::cast_dynamic with appropriate classes filled in. You can safely
 * add more cast_dynamic methods here as needed.
 */
%extend Glom::LayoutGroup {
static Glom::sharedptr<Glom::LayoutGroup> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutGroup *const pCppObject = dynamic_cast<Glom::LayoutGroup*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutGroup>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutGroup>();
}
};

%extend Glom::LayoutItem_Field {
static Glom::sharedptr<Glom::LayoutItem_Field> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_Field *const pCppObject = dynamic_cast<Glom::LayoutItem_Field*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_Field>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_Field>();
}
};

%extend Glom::LayoutItem_Text {
static Glom::sharedptr<Glom::LayoutItem_Text> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_Text *const pCppObject = dynamic_cast<Glom::LayoutItem_Text*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_Text>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_Text>();
}
};

%extend Glom::LayoutItem_Portal {
static Glom::sharedptr<Glom::LayoutItem_Portal> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_Portal *const pCppObject = dynamic_cast<Glom::LayoutItem_Portal*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_Portal>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_Portal>();
}
};

%extend Glom::LayoutItem_CalendarPortal {
static Glom::sharedptr<Glom::LayoutItem_CalendarPortal> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_CalendarPortal *const pCppObject = dynamic_cast<Glom::LayoutItem_CalendarPortal*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_CalendarPortal>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_CalendarPortal>();
}
};

%extend Glom::LayoutItem_Notebook {
static Glom::sharedptr<Glom::LayoutItem_Notebook> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_Notebook *const pCppObject = dynamic_cast<Glom::LayoutItem_Notebook*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_Notebook>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_Notebook>();
}
};

%extend Glom::LayoutItem_GroupBy {
static Glom::sharedptr<Glom::LayoutItem_GroupBy> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_GroupBy *const pCppObject = dynamic_cast<Glom::LayoutItem_GroupBy*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_GroupBy>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_GroupBy>();
}
};

%extend Glom::LayoutItem_Summary {
static Glom::sharedptr<Glom::LayoutItem_Summary> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_Summary *const pCppObject = dynamic_cast<Glom::LayoutItem_Summary*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_Summary>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_Summary>();
}
};

%extend Glom::LayoutItem_FieldSummary {
static Glom::sharedptr<Glom::LayoutItem_FieldSummary> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_FieldSummary *const pCppObject = dynamic_cast<Glom::LayoutItem_FieldSummary*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_FieldSummary>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_FieldSummary>();
}
};

%extend Glom::LayoutItem_VerticalGroup {
static Glom::sharedptr<Glom::LayoutItem_VerticalGroup> cast_dynamic(const Glom::sharedptr<Glom::LayoutItem>& src)
{
  Glom::LayoutItem_VerticalGroup *const pCppObject = dynamic_cast<Glom::LayoutItem_VerticalGroup*>(src.operator->());

  if(pCppObject)
    return Glom::sharedptr<Glom::LayoutItem_VerticalGroup>(pCppObject, src._get_refcount());
  else
    return Glom::sharedptr<Glom::LayoutItem_VerticalGroup>();
}
};

/*
 * Methods to access methods from Glom::UsesRelationship.
 *
 * These methods are needed because Java doesn't support multiple inheritance
 * and the glom_sharedptr macro has trouble with UsesRelationship. It's safe to
 * add more methods here. Adding these methods automatically with a python or
 * shell script might be an option if we want complete bindings.
 *
 * The comments have been included so that they will be coverted to Java
 * comments when Swig gets support for this. Comments are useful when using
 * Eclipse (or other IDEs).
 */
// The methods added to Glom::LayoutItem_Field are all of the public methods
// from Glom::UsesRelationship.
%extend Glom::LayoutItem_Field {

bool get_has_relationship_name()
{
  return self->get_has_relationship_name();
}

bool get_has_related_relationship_name()
{
  return self->get_has_related_relationship_name();
}

/** Convenience function, equivalent to get_relationship()->get_name().
 */
Glib::ustring get_relationship_name()
{
  return self->get_relationship_name();
}

/** Convenience function, equivalent to get_relationship()->get_name().
 */
Glib::ustring get_related_relationship_name()
{
  return self->get_related_relationship_name();
}

/** Return the relationship used by this item, if any, or a null sharedptr.
 * See also get_has_relationship_name() which can prevent the need for your
 * own null sharedptr check.
 */
sharedptr<const Relationship> get_relationship()
{
  return self->get_relationship();
}

void set_relationship(const sharedptr<const Relationship>& relationship)
{
  self->set_relationship(relationship);
}

/** Return the related relationship used by this item, if any, or a null sharedptr.
 * See also get_has_related_relationship_name() which can prevent the need for your
 * own null sharedptr check.
 */
sharedptr<const Relationship> get_related_relationship()
{
  return self->get_related_relationship();
}

void set_related_relationship(const sharedptr<const Relationship>& relationship)
{
  self->set_related_relationship(relationship);
}

/** Returns either the @a parent_table, related to table, or doubly-related to-table.
 */
Glib::ustring get_table_used(const Glib::ustring& parent_table)
{
  return self->get_table_used(parent_table);
}

/** Get the title of the relationship that is actually used,
 * falling back to the relationship's name.
 * @param parent_table_title The title of table to which the item (or its relationships) belong.
 */
Glib::ustring get_title_used(const Glib::ustring& parent_table_title, const Glib::ustring& locale)
{
  return self->get_title_used(parent_table_title, locale);
}

/** Get the singular title of the relationship that is actually used,
 * falling back to the regular (plural) title, and then to the relationship's name.
 * @param parent_table_title The title of table to which the item (or its relationships) belong.
 */
Glib::ustring get_title_singular_used(const Glib::ustring& parent_table_title, const Glib::ustring& locale)
{
  return self->get_title_singular_used(parent_table_title, locale);
}

Glib::ustring get_to_field_used()
{
  return self->get_to_field_used();
}

/** Get the name of the related relationship used, if any, or the relationship
 * if there is no related relationship, or an empty string if neither are
 * used by this item.
 */
Glib::ustring get_relationship_name_used()
{
  return self->get_relationship_name_used();
}

/** Discover whether the relationship used allows the user to edit values
 * in its to table.
 */
bool get_relationship_used_allows_edit()
{
  return self->get_relationship_used_allows_edit();
}

/** Get a name to use as an alias in SQL statements.
 * This will always be the same string for items that have the same definition.
 */
Glib::ustring get_sql_join_alias_name()
{
  return self->get_sql_join_alias_name();
}

/** Get the item's alias name, if it uses a relationship, or just get its table name.
 * @param parent_table The table to which the item (or its relatinoships) belong.
 */
Glib::ustring get_sql_table_or_join_alias_name(const Glib::ustring& parent_table)
{
  return self->get_sql_table_or_join_alias_name(parent_table);
}

};


// The methods added to Glom::LayoutItem_Portal are all of the public methods
// from Glom::UsesRelationship.
%extend Glom::LayoutItem_Portal {

bool get_has_relationship_name()
{
  return self->get_has_relationship_name();
}

bool get_has_related_relationship_name()
{
  return self->get_has_related_relationship_name();
}

/** Convenience function, equivalent to get_relationship()->get_name().
 */
Glib::ustring get_relationship_name()
{
  return self->get_relationship_name();
}

/** Convenience function, equivalent to get_relationship()->get_name().
 */
Glib::ustring get_related_relationship_name()
{
  return self->get_related_relationship_name();
}

/** Return the relationship used by this item, if any, or a null sharedptr.
 * See also get_has_relationship_name() which can prevent the need for your
 * own null sharedptr check.
 */
sharedptr<const Relationship> get_relationship()
{
  return self->get_relationship();
}

void set_relationship(const sharedptr<const Relationship>& relationship)
{
  self->set_relationship(relationship);
}

/** Return the related relationship used by this item, if any, or a null sharedptr.
 * See also get_has_related_relationship_name() which can prevent the need for your
 * own null sharedptr check.
 */
sharedptr<const Relationship> get_related_relationship()
{
  return self->get_related_relationship();
}

void set_related_relationship(const sharedptr<const Relationship>& relationship)
{
  self->set_related_relationship(relationship);
}

/** Returns either the @a parent_table, related to table, or doubly-related to-table.
 */
Glib::ustring get_table_used(const Glib::ustring& parent_table)
{
  return self->get_table_used(parent_table);
}

/** Get the title of the relationship that is actually used,
 * falling back to the relationship's name.
 * @param parent_table_title The title of table to which the item (or its relationships) belong.
 */
Glib::ustring get_title_used(const Glib::ustring& parent_table_title, const Glib::ustring& locale)
{
  return self->get_title_used(parent_table_title, locale);
}

/** Get the singular title of the relationship that is actually used,
 * falling back to the regular (plural) title, and then to the relationship's name.
 * @param parent_table_title The title of table to which the item (or its relationships) belong.
 */
Glib::ustring get_title_singular_used(const Glib::ustring& parent_table_title, const Glib::ustring& locale)
{
  return self->get_title_singular_used(parent_table_title, locale);
}

Glib::ustring get_to_field_used()
{
  return self->get_to_field_used();
}

/** Get the name of the related relationship used, if any, or the relationship
 * if there is no related relationship, or an empty string if neither are
 * used by this item.
 */
Glib::ustring get_relationship_name_used()
{
  return self->get_relationship_name_used();
}

/** Discover whether the relationship used allows the user to edit values
 * in its to table.
 */
bool get_relationship_used_allows_edit()
{
  return self->get_relationship_used_allows_edit();
}

/** Get a name to use as an alias in SQL statements.
 * This will always be the same string for items that have the same definition.
 */
Glib::ustring get_sql_join_alias_name()
{
  return self->get_sql_join_alias_name();
}

/** Get the item's alias name, if it uses a relationship, or just get its table name.
 * @param parent_table The table to which the item (or its relationships) belong.
 */
Glib::ustring get_sql_table_or_join_alias_name(const Glib::ustring& parent_table)
{
  return self->get_sql_table_or_join_alias_name(parent_table);
}

};
