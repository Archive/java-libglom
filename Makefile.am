## Copyright (c) 2009 - 2012 Openismus GmbH
##
## java_libglom is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## java_libglom is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with java_libglom; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

## This project does not use recursive make, but a single toplevel Makefile
## to build the entire tree (excluding the po subdirectory as gettext comes
## with its own build system).  Read Peter Miller's excellent paper to learn
## why recursive make invocations are both slow and error-prone:
## http://members.pcug.org.au/~millerp/rmch/recu-make-cons-harm.html

AUTOMAKE_OPTIONS = subdir-objects
ACLOCAL_AMFLAGS  = -I m4 ${ACLOCAL_FLAGS}
DISTCHECK_CONFIGURE_FLAGS = --enable-warnings=fatal

SUBDIRS =

jarfile = java-libglom-@PACKAGE_VERSION@.jar
doc_jarfile = java-libglom-@PACKAGE_VERSION@-javadoc.jar
sources_jarfile = java-libglom-@PACKAGE_VERSION@-sources.jar
jardir = $(datadir)/java

generated_java_sources = src/main/java/org/glom/libglom/BakeryDocument.java \
                         src/main/java/org/glom/libglom/BakeryDocumentXML.java \
                         src/main/java/org/glom/libglom/ChoiceValue.java \
                         src/main/java/org/glom/libglom/ChoiceValueVector.java \
                         src/main/java/org/glom/libglom/CustomTitle.java \
                         src/main/java/org/glom/libglom/DoubleVector.java \
                         src/main/java/org/glom/libglom/Document.java \
                         src/main/java/org/glom/libglom/Formatting.java \
                         src/main/java/org/glom/libglom/Field.java \
                         src/main/java/org/glom/libglom/FieldVector.java \
                         src/main/java/org/glom/libglom/FoundSet.java \
                         src/main/java/org/glom/libglom/GlomConstants.java \
                         src/main/java/org/glom/libglom/Glom.java \
                         src/main/java/org/glom/libglom/GlomJNI.java \
                         src/main/java/org/glom/libglom/HasTitleSingular.java \
                         src/main/java/org/glom/libglom/LayoutFieldVector.java \
                         src/main/java/org/glom/libglom/LayoutGroup.java \
                         src/main/java/org/glom/libglom/LayoutGroupVector.java \
                         src/main/java/org/glom/libglom/LayoutItem_CalendarPortal.java \
                         src/main/java/org/glom/libglom/LayoutItem_Field.java \
                         src/main/java/org/glom/libglom/LayoutItem_FieldSummary.java \
                         src/main/java/org/glom/libglom/LayoutItem.java \
                         src/main/java/org/glom/libglom/LayoutItem_GroupBy.java \
                         src/main/java/org/glom/libglom/LayoutItem_Notebook.java \
                         src/main/java/org/glom/libglom/LayoutItem_Portal.java \
                         src/main/java/org/glom/libglom/LayoutItem_Summary.java \
                         src/main/java/org/glom/libglom/LayoutItem_Text.java \
                         src/main/java/org/glom/libglom/LayoutItem_VerticalGroup.java \
                         src/main/java/org/glom/libglom/LayoutItemVector.java \
                         src/main/java/org/glom/libglom/LayoutItem_WithFormatting.java \
                         src/main/java/org/glom/libglom/LocalToTranslationMap.java \
                         src/main/java/org/glom/libglom/NumericFormat.java \
                         src/main/java/org/glom/libglom/PrintLayout.java \
                         src/main/java/org/glom/libglom/Relationship.java \
                         src/main/java/org/glom/libglom/RelationshipVector.java \
                         src/main/java/org/glom/libglom/Report.java \
                         src/main/java/org/glom/libglom/SortClause.java \
                         src/main/java/org/glom/libglom/SortFieldPair.java \
                         src/main/java/org/glom/libglom/SqlBuilder.java \
                         src/main/java/org/glom/libglom/SqlExpr.java \
                         src/main/java/org/glom/libglom/SqlOperatorType.java \
                         src/main/java/org/glom/libglom/StringVector.java \
                         src/main/java/org/glom/libglom/TableInfo.java \
                         src/main/java/org/glom/libglom/TableInfoVector.java \
                         src/main/java/org/glom/libglom/TranslatableItem.java \
                         src/main/java/org/glom/libglom/TranslatableItemPair.java \
                         src/main/java/org/glom/libglom/TranslatableItemVector.java \
                         src/main/java/org/glom/libglom/TypeNameMap.java \
                         src/main/java/org/glom/libglom/Value.java \
                         src/main/java/org/glom/libglom/ValueVector.java \
                         src/main/java/org/glom/libglom/ValueVectorPair.java \
                         src/main/java/org/glom/libglom/ValueWithSecondVector.java


swig_sources = src/glom.i \
               src/libgdamm.i \
               src/glib_refptr.i \
               src/glib_ustring.i \
               src/glom_constants.i.in \
               src/glom_sharedptr.i

java_binaries = $(top_builddir)/$(jarfile) \
                $(top_builddir)/$(doc_jarfile) \
                $(top_builddir)/$(sources_jarfile) \
                $(top_builddir)/org/glom/libglom/*.class \
                $(top_builddir)/src/test/java/org/glom/libglom/tests/*.class \
                $(top_builddir)/src/examples/org/glom/libglom/examples/*.class

if MAINTAINER_MODE
CLEANFILES = $(java_binaries) tests-and-examples.stamp $(generated_java_sources) $(top_builddir)/src/glom_wrap.cc
swig_dependencies = $(swig_sources)
else
CLEANFILES = $(java_binaries) tests-and-examples.stamp
swig_dependencies =
endif

#We use this special target because CLEANFILES cannot contain a whole directory.
clean-local:
	$(RM) -rf $(top_builddir)/doc

dist_noinst_JAVA = $(generated_java_sources)

test_sources = src/test/java/org/glom/libglom/tests/DocumentTest.java \
               src/test/java/org/glom/libglom/tests/FakeConnectionTest.java \
               src/examples/org/glom/libglom/examples/ExampleDocumentLoad.java
             
EXTRA_DIST = $(swig_sources) \
             $(test_sources)

TESTS = tools/JUnitTests tools/ExampleDocumentLoad

# TODO: How is c++ converted to the base variable name?
# lib_LTLIBRARIES = src/main/c++/libjava_glom-0.1.la
lib_LTLIBRARIES = libjava_libglom-@PACKAGE_VERSION@.la

libjava_libglom_@PACKAGE_VERSION@_la_SOURCES = src/glom_wrap.cc
libjava_libglom_@PACKAGE_VERSION@_la_LDFLAGS = $(LIBGLOM_LIBS) -release @PACKAGE_VERSION@

# We use -fno-strict-aliasing because SWIG recommends it here:
# http://www.swig.org/Doc2.0/Java.html#Java_compiling_dynamic
AM_CPPFLAGS =  -I$(top_builddir) $(LIBGLOM_CFLAGS) $(JAVA_LIBGLOM_WARNING_FLAGS) -fno-strict-aliasing

src/glom_wrap.cc: $(swig_dependencies)
	$(MKDIR_P) $(top_builddir)/src/main/java/org/glom/libglom
	$(SWIG) -Wall -java -package org.glom.libglom \
	  -I$(LIBGLOM_INCLUDE_DIR)/glom-1.22 \
	  -I$(LIBGDAMM_INCLUDE_DIR)/libgdamm-5.0 \
	  -I$(GLIBMM_INCLUDE_DIR)/glibmm-2.4 \
	  -outdir $(top_builddir)/src/main/java/org/glom/libglom \
	  -o $(top_builddir)/src/glom_wrap.cc $(srcdir)/src/glom.i
# 	Add synchronization to all JNI methods. I can't figure out how to get Swig to do this.
#	This fixes the failure with the testThreadedAccess() method.
	sed -i -e "s/public final/public synchronized final/" \
	  src/main/java/org/glom/libglom/GlomJNI.java

# This use of the .stamp files feels like a hack.
# This used to depend on  classdist_noinst.stamp but that is no longer generated,
# at least with automake 1.11. murrayc.
$(jarfile): classnoinst.stamp
	$(JAR) cf $(JARFLAGS) $(top_builddir)/$(jarfile) org

all-local: $(jarfile) $(doc_jarfile) $(sources_jarfile)  tests-and-examples.stamp

# This uses mvn to install the .jar file in the local .m2 directory.
# and install our .pom file alongside it.
pomfile = java-libglom-@PACKAGE_VERSION@.pom
install-data-local:
	test -z "$(jardir)" || $(MKDIR_P) "$(DESTDIR)$(jardir)"
	$(INSTALL_DATA) $(jarfile) "$(DESTDIR)$(jardir)"
	test -z $(MVN) || $(MVN) install:install-file -Dfile=$(jarfile) -DpomFile=$(pomfile) -DgeneratePom=false

uninstall-local:
	rm -f "$(DESTDIR)$(jardir)/$(jarfile)"

tests-and-examples.stamp: $(jarfile) $(test_sources)
	$(MKDIR_P) $(top_builddir)/src/test/java
	$(JAVAC) -cp $(top_builddir)/$(jarfile):$(JUNIT_JAR):$(HAMCREST_CORE_JAR) \
	  -d $(top_builddir)/src/test/java $(AM_JAVACFLAGS) $(JAVACFLAGS) \
	  $(top_srcdir)/src/test/java/org/glom/libglom/tests/DocumentTest.java \
	  $(top_srcdir)/src/test/java/org/glom/libglom/tests/FakeConnectionTest.java
	sed -i -e "s/@JARFILE@/$(jarfile)/" tools/JUnitTests
	chmod 0755 tools/JUnitTests
	$(MKDIR_P) $(top_builddir)/src/examples
	$(JAVAC) -cp $(top_builddir)/$(jarfile) -d $(top_builddir)/src/examples \
	  $(AM_JAVACFLAGS) $(JAVACFLAGS) \
	  $(top_srcdir)/src/examples/org/glom/libglom/examples/ExampleDocumentLoad.java
	sed -i -e "s/@JARFILE@/$(jarfile)/" tools/ExampleDocumentLoad
	chmod 0755 tools/ExampleDocumentLoad
	@echo "java-libglom tests and examples generated and built." > tests-and-examples.stamp

# Upload to the sonatype.org repository:
# After upload, you need to "Close" it and then "Release" it in the UI here: https://oss.sonatype.org/
# This is then automatically synchronized to the central maven repository,
# within around 2 hours:
# http://search.maven.org/#search|ga|1|a%3A%22java-libglom%22
# so people can then just mention it as a regular <dependency> in maven pom.xml files,
# as we do in gwt-glom.
# That was originally setup like so:
# https://issues.sonatype.org/browse/OSSRH-2629?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel
deploy: $(pomfile) $(jarfile) 
	mvn gpg:sign-and-deploy-file -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ -DrepositoryId=sonatype-nexus-staging -DpomFile=$(pomfile) -Dfile=$(jarfile)
	mvn gpg:sign-and-deploy-file -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ -DrepositoryId=sonatype-nexus-staging -DpomFile=$(pomfile) -Dfile=$(sources_jarfile) -Dclassifier=sources
	mvn gpg:sign-and-deploy-file -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ -DrepositoryId=sonatype-nexus-staging -DpomFile=$(pomfile) -Dfile=$(doc_jarfile) -Dclassifier=javadoc


docbuild: $(jarfile)
	javadoc -public $(top_srcdir)/src/main/java/org/glom/libglom/*.java -d doc

$(doc_jarfile): docbuild
	$(JAR) cf $(JARFLAGS) $(top_builddir)/$(doc_jarfile) $(top_builddir)/doc/*

$(sources_jarfile): $(generated_java_sources)
	$(JAR) cf $(JARFLAGS) $(top_builddir)/$(sources_jarfile) $(top_srcdir)/src/main/java

